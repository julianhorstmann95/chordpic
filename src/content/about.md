---
layout: page
title: 'About'
---

ChordPic is a completely free tool to create guitar chord charts.

While many tools exist to create guitar chord charts, ChordPic is by far the fastest and easiest solution.

## Contribute

ChordPic is completely open source. [The code of this website is available on GitLab](https://gitlab.com/Voellmy/chordpic). If you are a developer and want to improve this web site, please feel free to create a pull request.

## Feature Requests or Bug Reports

If you're missing an essential feature or found a bug, [please create a ticket on GitLab](https://gitlab.com/Voellmy/chordpic/issues) or [write us an email](mailto:incoming+voellmy-chordpic-13938802-issue-@incoming.gitlab.com).

## Privacy Notice

<a href="/privacy-notice">Read ChordPic's privacy notice here</a>.

## Cookie Policy

<a href="/cookie-policy">Read ChordPic's cookie policy or adjust your settings here</a>.
